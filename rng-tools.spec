Name:            rng-tools
Version:         6.17
Release:         1
Summary:         Random number generator daemon
License:         GPL-2.0-or-later
URL:             https://github.com/nhorman/rng-tools
Source0:         https://github.com/nhorman/rng-tools/archive/v%{version}.tar.gz
Source2:         rngd.sysconfig

# Modified according to openSUSE and archlinux
Patch0:          rng-tools-service-harden.patch

# upstream patches
Patch1001:       backport-rng-tools-6.17-intel-cet-ibt-instrumentation.patch

# Dependency
BuildRequires: gcc make
BuildRequires: autoconf automake libtool
BuildRequires: pkgconfig(jansson)
BuildRequires: pkgconfig(libcap)
BuildRequires: pkgconfig(libcrypto)
BuildRequires: pkgconfig(libcurl)
BuildRequires: pkgconfig(libp11)
BuildRequires: pkgconfig(libxml-2.0)
BuildRequires: pkgconfig(openssl)
BuildRequires: jitterentropy-library-devel

# Provide the command: killall used by test cases.
BuildRequires: /usr/bin/killall
BuildRequires: opensc

Requires: opensc
%{?systemd_requires}

%description
Rng-tools is a random number generator daemon.It monitors a set of entropy sources,
and supplies entropy from them to the system kernel's /dev/random machinery.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
./autogen.sh
# a dirty hack so libdarn_impl_a_CFLAGS overrides common CFLAGS
sed -i -e 's/$(libdarn_impl_a_CFLAGS) $(CFLAGS)/$(CFLAGS) $(libdarn_impl_a_CFLAGS)/' Makefile.in
%configure --without-rtlsdr
%make_build

%install
%make_install

install -D -m 0644 rngd.service %{buildroot}%{_unitdir}/rngd.service
# install sysconfig file
install -D %{S:2} -m0644 %{buildroot}%{_sysconfdir}/sysconfig/rngd

%check
export RNGD_JITTER_TIMEOUT=10 #Enseur that the AES can be generated.
%make_build check

%post
%systemd_post rngd.service

%preun
%systemd_preun rngd.service

%postun
%systemd_postun_with_restart rngd.service

%files
%license COPYING
%doc AUTHORS NEWS README
%{_bindir}/rngtest
%{_bindir}/randstat
%{_sbindir}/rngd
%{_unitdir}/rngd.service
%config(noreplace) %{_sysconfdir}/sysconfig/rngd

%files help
%{_mandir}/man1/rngtest.1.*
%{_mandir}/man8/rngd.8.*

%changelog
* Sat Oct 05 2024 Funda Wang <fundawang@yeah.net> - 6.17-1
- update to 6.17
- drop unused dependencies

* Mon Apr 24 2023 zhangruifang <zhangruifang1@h-partners.com> - 6.16-3
- Update the rngd.service file
- Add the config file for rngd service

* Fri Apr 21 2023 zhangruifang <zhangruifang1@h-partners.com> - 6.16-2
- enable make check

* Sat Jan 28 2023 fuanan <fuanan3@h-partners.com> - 6.16-1
- update version to 6.16

* Fri Sep 2 2022 panxiaohe <panxh.life@foxmail.com> - 6.14-4
- fix changelog to make it in descending chronological order

* Thu Jul 28 2022 zhangruifang <zhangruifang1@h-partners.com> - 6.14-3
- update release

* Tue Jan 18 2022 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 6.14-2
- Add requires:opensc,prevent service startup failure logs

* Wed Dec 29 2021 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 6.14-1
- update version to 6.14

* Sat Dec 19 2020 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 6.5-3
- fix rngd.service coredump

* Thu Dec 10 2020 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 6.5-2
- fix rngd.service coredump

* Wed Sep 2 2020 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 6.5-1
- since 6.6, jitterentropy-library is independed from rng-tools,
  no any entropy source will lead to rng-tools service fail.

* Thu Jul 30 2020 zhangxingliang <zhangxingliang3@huawei.com> - 6.10-1
- update to 6.10

* Sat Feb 29 2020 openEuler Buildteam <buildteam@openeuler.org> - 6.3.1-4
- add jitterentropy support

* Wed Feb 19 2020 wanjiankang <wanjiankang@huawei.com> - 6.3.1-3
- Package init
